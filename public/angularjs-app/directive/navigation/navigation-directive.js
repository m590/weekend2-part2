angular.module('meanGames').directive("gamesNavigation", GamesNavigation);

function GamesNavigation(){
    return {
        restrict: "E",
        templateUrl: "angularjs-app/directive/navigation/navigation.html"
    }
}