meanGames.directive("gameRating", GameRating);

function GameRating() {
    return {
        restrict: "E",
        templateUrl: "angularjs-app/game-display/rating.html",
        bindToController: true,
        controller: "GameController",
        controllerAs: "vm"
    }
}