meanGames.factory("GameService", GameService);

const _url = "/api/games";

function GameService($http) {
    return {
        getAllGames: getAll,
        getGame: getOne,
        addGame: addOne,
        updateGame: updateOne,
        deleteGame: deleteOne
    }

    function getAll() {
        return $http.get(_url).then(complete).catch(failed);
    }

    function getOne(id) {
        return $http.get(_url + "/" + id).then(complete).catch(failed);
    }

    function addOne(game) {
        return $http.post(_url, game).then(complete).catch(failed);
    }

    function updateOne(game) {
        return $http.put(_url + "/" + game._id, game).then(complete).catch(failed);
    }

    function deleteOne(id) {
        return $http.delete(_url + "/" + id).then(complete).catch(failed);
    }

    function complete(res) {
        return res.data;
    }

    function failed(error) {
        return error;
    }
}