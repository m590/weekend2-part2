angular.module('meanGames').factory("UserFactory", UserFactory);

function UserFactory($http) {
    return {
        register: register,
        login: login
    }

    function register(user) {
        return $http.post("/api/user/register", user).then(complete).catch(failure);
    }

    function login(user) {
        return $http.post("/api/user/login", user).then(complete).catch(failure);
    }

    function complete(response) {
        return response.data;
    }

    function failure(error) {
        return error;
    }
}