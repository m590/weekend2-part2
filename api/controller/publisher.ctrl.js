const mongoose = require('mongoose');
const Game = mongoose.model('Game');

module.exports.publisherGetOne = function (req, res) {
    const gameId = req.params.gameId;

    Game.findById(gameId).select("publisher").exec(function (err, game) {
        console.log("game", game);
        const publisher = game.publisher;
        res.status(200).json(publisher);
    });
}

const _addPublisher = function (req, res, game) {
    game.publisher.name = req.body.name;
    game.publisher.location.type = "Point";
    game.publisher.location.coordinates = [parseFloat(req.body.lng), parseFloat(req.body.lat)];

    console.log('publisher', game.publisher);
    game.save(function (err, savedGame) {
        const response = {
            status: 201,
            message: savedGame
        };

        if (err) {
            console.log('Error creating publisher', err);
            response.status = 500;
            response.message = 'Internal error';
        }
        res.status(response.status).json(response.message);
    });
}

module.exports.publisherAddOne = function (req, res) {
    const gameId = req.params.gameId;

    Game.findById(gameId).exec(function (err, game) {
        console.log("game", game);
        const response = {
            status: 201,
            message: game
        };
        if (err) {
            response.status = 500;
            response.message = err;
        } else if (!game) {
            response.status = 404;
            response.message = 'Game not found';
        }
        if (response.status === 201) {
            _addPublisher(req, res, game);
        } else {
            res.status(response.status).json(response.message);
        }
    });
}
