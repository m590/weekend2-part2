const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = mongoose.model("User");

module.exports.register = function (req, res) {

    if (!req.body.name || !req.body.username || !req.body.password) {
        res.status(400).json({message: "name, username, password fields are required"});
        return;
    }

    const response = {status: 201, content: {message: 'Ok'}};
    bcrypt.genSalt(10, 0, function (err, salt) {
        if (salt) {
            bcrypt.hash(req.body.password, salt, function (err, hashPassword) {
                if (hashPassword) {
                    const newUser = {
                        name: req.body.name,
                        username: req.body.username,
                        password: hashPassword
                    }
                    User.create(newUser, function (err, savedUser) {
                        if (savedUser) {
                            response.status = 201;
                            response.content = {message: "Created"};
                            res.status(response.status).json(response.content);
                        } else {
                            console.log('UserCtrl.register couldn\'t save user', err);
                            response.status = 500;
                            response.content = {message: "Internal error"};
                            res.status(response.status).json(response.content);
                        }
                    });
                } else {
                    console.log('UserCtrl.register couldn\'t save user', err);
                    response.status = 500;
                    response.content = {message: "Internal error"};
                    res.status(response.status).json(response.content);
                }
            });
        } else {
            console.log('UserCtrl.register couldn\'t create salt', err);
            response.status = 500;
            response.content = {message: "Internal error"};
            res.status(response.status).json(response.content);
        }
    });
}

module.exports.login = function (req, res) {
    if (!req.body.username || !req.body.password) {
        res.status(400).json({message: "username, password fields are required"});
        return;
    }

    const response = {};
    const query = {
        username: req.body.username
    }
    User.findOne(query).exec(function (err, foundUser) {
        if (foundUser) {
            bcrypt.compare(req.body.password, foundUser.password, function (err, same) {
                if (same) {
                    const token = jwt.sign({name: foundUser.name}, process.env.pass_phrase, {expiresIn: 3600});
                    response.status = 200;
                    response.content = {success: true, token: token};
                    res.status(response.status).json(response.content);
                } else {
                    if (err) {
                        console.log('UserCtrl.login comparing password error', err);
                    }
                    response.status = 400;
                    response.content = {message: "Wrong credential"};
                    res.status(response.status).json(response.content);
                }
            });
        } else {
            if (err) {
                console.log('UserCtrl.register couldn\'t save user', err);
                response.status = 500;
                response.content = {message: "Internal error"};
                res.status(response.status).json(response.content);
            } else {
                response.status = 400;
                response.content = {message: "Wrong credential"};
                res.status(response.status).json(response.content);
            }

        }
    });
}

module.exports.authenticate = function (req, res, next) {
    const headerAuth = req.headers.authorization;
    if (headerAuth) {
        const token = headerAuth.split(' ')[1];
        jwt.verify(token, process.env.pass_phrase, function (err, decoded) {
            if (decoded) {
                next();
            } else {
                if (err) {
                    console.log('user.ctrl authenticate error', err);
                }
                res.status(401).json({message: 'Unauthorized'});
            }
        });
    } else {
        res.status(401).json({message: 'Unauthorized'});
    }
}